# QuaID: Enabling Earlier Detection of Recently Emerged SARS-CoV-2 Variants of Concern in Wastewater

## Summary

Highly sensitive detection of emerging variants remains a complex task due to the pooled nature of environmental samples and genetic material degradation. This repository contains an implementation of a novel bioinformatics tool (QuaID) for VoC detection based on quasiunique mutations. The benefits of QuaID are three-fold: 

1. provides up to 3 week earlier VoC detection compared to existing approaches
2. enables more sensitive VoC detection, which is shown to be tolerant of >50% mutation drop-out
3. leverages all mutational signatures, including insertions & deletions.

## 3rd party software requirements 

Below is the list of 3rd party software requirements. All required sofwtare can be installed via Miniconda after adding `bioconda` to the list of channels. Version specified in the parantheses is the version currently tested.

* bbmap (38.90)
* bwa (0.7.17)
* fastqc (0.11.9)
* ivar (1.3.1)
* lofreq (2.1.5)
* samtools (1.7)
* vdb (2.7)

## Python requirements

* numpy (1.19.0)
* pandas (1.0.5)
* pyvcf 
* tqdm

## Creating conda environment for running QuaID

The easiest and quickest way to prepare an environment for running QuaID is by running the following commands: 
```
conda create -n quaid-env --file quaid-env-list.txt
conda activate quaid-env
```

## Operating system requirements

The code has been tested on MacOS Monterey 12.4 (Intel MacBook) and Ubuntu 18.04.6 (kernel version 4.15.0-171-generic).

## Overview of the pipeline

![](imgs/Pipeline-overview.png)

## Usage

### MSA preprocessing and database construction

Before running QuaID you will need a database of mutations and corresponding SARS-CoV-2 lineages. In order to extract this information from a GISAID multiple sequence alignment (MSA) file we rely on `vdb` ([GitHub link](https://github.com/variant-database/vdb)). 

Since the current MSAs are very large files we will not extract them from the archive, but process them in a streamign fashion with `vdb` as follows:
```
tar -xOf hCoV-19_msa_0811.tar.xz msa_0811/msa_0811.fasta | ./vdbCreate -s -N
./vdb -t vdb_081123_nucl.txt vdb_081123_trimmed_nucl.txt
```
The `-s` flag here specifies the streaming processing, and the `-N` flag instructs `vdb` to extract nucleotide level mutation information. The second command trims the resulting output removing mutations infered due to ambiguous bases in incomplete assemblies.

The `vdb_081123_trimmed_nucl.txt` file is now ready to be processed into a suitable QuaID database. In case if you are unable to install or run `vdb` we also provide this file based on the 08/11/2023 GISAID MSA [here](https://rice.app.box.com/s/807z4abci1wpm6xdpe889n4ujm0eos2i). In order to generate a QuaID suitable DB use the following command:
```
python vdb-to-df.py vdb_081123_trimmed_nucl.txt metadata.tsv
```

The output will be stored in the `vdb_output` folder by default. 
**Note:** You need `dask` Python package in order to succesfully run `vdb-to-df.py`, since the dataframe resulting from the operation is too big for regular `pandas` processing. 

### File tree structure

Input data should be grouped by sample date (folder) with samples as sub-folder. Each read file must include R1/R2 in the filename to indicate which reads from the pair it contains.

```
Data/
|
 -- Date/ [ex. 2022-04-20]
    |
     -- Sample/ [ex. Sample-1]
        |
         -- read_file_R1.fq
         -- read_file_R2.fq
     -- ...
 -- ... 
```

All intermediate folders should follow that same srructure (automatically created based of the `Data` folder) of `Folder-type/Date/Sample/files.*`.

### Example pipeline execution

After the input data is organized as described above, the main pipeline pre-processing script should be ran as

```
./run_analyses.sh Date [ex. 2022-04-20]
```

Once the script finishes the folder `Variant-calling-combined` should have the subfolder `Date` with files `Sample-XXX-*.tsv`. If this folder is empty two scenarios are likely: (1) parts of the pipeline have failed while pre-processing the data; (2) data contained no variants that were called by both LoFreq and iVar. The second scenario is usually easy to verify by checking individual outputs of LoFreq and iVar located in the folders `Variant-calling-LoFreq` and `Varianr-calling-iVar` respectively.

### Example QuaID execution

After the pre-processing pipeline finishes execution we can run the analysis script. In addition to the output from data pre-processing step this script also requires some "database" files (in the command below these are the files in the `MSA_precomputed` folder) that can be pre-computed ahead of time. The `level` argument refers to the level of PANGO hierarchy at which the quasi-unique mutations are aggregated, for example `--level 4` means that all mutations associated with sublineages of B.1.1.529 will end aggregated and the detection events will be reported as B.1.1.529 detections. Currently we support levels 4 and 5 of the hierachy, since all current variants of concern reside within these two levels. However, adjusting for reporting at other levels is straightforward.

```
python quaid.py MSA_precomputed/vdb_lineage_df_week.csv MSA_precomputed/metadata.tsv Variant-calling-combined Coverage --concat-output --occurence-cutoff 2 --inclusion-cutoff 0.5 --exclusion-cutoff 0.5 --time-window 4 --level 4 --verbose
```

If executed in an interactive session, an optional `--verbose` flag can be provided to print progress log to `stdout`. Otherwise, the script will run in quiet mode without printing anything to `stdout`, and instead producing a log file.

### QuaID command line parameters

```
usage: quaid.py [-h] [--output-folder OUTPUT_FOLDER] [--concat-output] [--occurence-cutoff OCCURENCE_CUTOFF] [--inclusion-cutoff INCLUSION_CUTOFF] [--exclusion-cutoff EXCLUSION_CUTOFF]
                [--time-window TIME_WINDOW] [--level LEVEL] [--save_qu_to_disk SAVE_QU_TO_DISK] [--save-vcf] [--generate_plots] [--verbose]
                vdb_path metadata_path vcf_folder_path coverage_folder

positional arguments:
  vdb_path              path to the aggregated VDB output data
  metadata_path         path to the metadata file
  vcf_folder_path       path to the folder containing variant calling information
  coverage_folder       path to the folder containing coverage information

optional arguments:
  -h, --help            show this help message and exit
  --output-folder OUTPUT_FOLDER
                        path to the output folder
  --concat-output       create a single summary table instead of separate files per date
  --occurence-cutoff OCCURENCE_CUTOFF [default: 0]
                        minimum number of genomes required per lineage (lineages below this count are excluded)
  --inclusion-cutoff INCLUSION_CUTOFF [default: 0.5]
                        minimum mutation prevalence fraction in a lineage for the inclusion into the quasi-unique set
  --exclusion-cutoff EXCLUSION_CUTOFF
                        minimum mutation prevalence fraction in a non-target lineage used for exclusion from the quasi-unique set
  --time-window TIME_WINDOW [default: 4]
                        number of prior weeks of assembly data to consider (or -1 to use YTD)
  --level LEVEL         [default: 4]
                        PANGO lineage level for quasi-unique mutations (e.g. B.1.1.7 is level 4)
  --save_qu_to_disk SAVE_QU_TO_DISK [default: None]
                        path to a file where to save quasi-unique mutations DataFrame, if None the file is not saved to disk
  --save-vcf            save individual mutation calls with lineage annotation
  --verbose             print progress information to stdout (useful for progress monitoring)
```

#### --inclusion-cutoff and --exclusion-cutoff

`--inclusion-cutoff` and `--exclusion-cutoff` parameters control how specific the mutations recruited into the quasi-unqiue set for a given lineage are. Higher values of the `--inclusion-cutoff` imply that the higher fraction of the mutations belonging to the target lineage must contain the quasi-unique mutation. In particular, setting this value 1 will require all genomes in a given lineage to contain each of the quasi-unique mutations. It is not advised to set this value to 1 unless utilizing a well-curated sequence database, since mis-assemblies and unresovled regions in database genomes can bring the fraction of genomes containing quasi-unqiue mutation in a lineage below 100%. Lower values of the `--exclusion-cutoff` also increase specificity of detection, since they imply that mutations found in at least `--exclusion-cutoff` fraction of genomes in any lineage other than the target one will be excluded from the quasi-unique set. Low values of `--exclusion-cutoff` are particularly sensitive to the presence of small (in the number of genomes) lineages in the sequence database, since a single misclassified genome from the target lineage appearing in a different small lineage can bring the mutation prevalence over the `--exclusion-cutoff` in that case.

We have found empirically that the values `--inclusion-cutoff 0.5 --exclusion-cutoff 0.5` offer great sensitivity without sacrificing specificity for the variant detection problem. However, alternative parameters setting such as `--inclusion-cutoff 0.8 --exclusion-cutoff 0.5` and `--inclusion-cutoff 0.8 --exclusion-cutoff 0.2` can be used when specificity of detection is a greater concern than sesnsitivity. We also note that more stringent settings lead to smaller quasi-unqiue sets of mutations which can lead to degraded performance on fragmented and/or low viral load samples. 

#### --occurence-cutoff

In certain scenarios it might be beneficial to preclude lineages with very small number of genomes from being taken into account when determining quasi-unqiue mutations. For this purpose we added the `--occurence-cutoff` option which allows user to set a minimum number of genomes a lineage mus thave to be considered for the quasi-unique mutation set determination. One common case when this setting is useful is a scanerio in which <10 genomes belonging to some lineage appear in the database for the selected time frame. In such cases it is possible for such small lineage to have a high (>0.5) fraction of genomes containing a mutation even if that mutation is not characteristic for the lineage. 

In our analyses we set this parameter to 0, but users interested in using this cutoff as an ad-hoc quality control tool for their database can set it to a non-zero value. However, we suggest to not set `--occurence-cutoff` to value larger than 10, as that can lead to exclusion of important emerging lineages.

#### --time-window

In order to increase sensitivity of our appraoch with respect to newly emerging lineages we employ the `--time-window X` parameter which given a sample date (inferred from the experiment folder name) only utilizes the X weeks of sequence database data preceding the sample collection date. By limiting our seaerch for quasi-unique mutations to the recent sequence data we improve sensitivity since mutations that would not be considered quasi-unique due to appearance in a historically distant lineage are allowed in this setting. 

We found that in the dense sampling scenario a window of 4 weeks offers great sensivity without impact on specificity of detections. However, as the sampling density decreases it can be useful to extend the window size to allow more historical data to be considered.

### Output structure

A report file will be generated in the specified output directory (flag `--output output_directory_path` with defautl value `./output`). Output file name will include date range for the samples, inclusion, exclusion, and occurence thresholds, time window, and level used in the run. Output file will be in comma-separated values (CSV) format and will contain the following columns:

| Date | Plant | Total AF | Total QU count | # QU possible | Fraction covered | WHO name |
|------|-------|----------|----------------|---------------|------------------|----------|
| Sample date | Sample name | Sum of the allele frequencies of detected QU mutations (a floating point value in the range [0., Total QU count]) | Count of detected QU mutations (i.e. the size of the set intersection between mutations designated as QU for the variant, and mutations occuring in a sample) | Total number of possible QU mutations for the given variant (if the selected time window in GISAID contains no sequences from the given variant or the inclusion/exclusion thresholds are set too stringently this value can be 0) | Fraction of QU mutation sites with non-zero coverage (flanking coverage for indels). Note that this only checks the coverage of the site itself, and hence does not guarantee that a mutation was called (e.g. site with non-zero coverage, but less than 5x coverage or site with allele frequency below 0.02). | WHO name for the considered VoC (e.g. Delta) or PANGO lineage name (e.g. BA.5) |

### Demo run 

In order to run minimal complete example you will first need to download the precomputed database files (which otherwise can be generated via the `vdb-to-df.py` script) from [here](https://rice.box.com/s/a7wwf0cg79upnogynd4sxty9rrpwkzus). You will also need a set of samples. For the demo run we recommend using our pre-generated set of simulated sequencing reads that can be downloaded from [here](https://rice.box.com/s/qj4bny5n93kylp4cunfz20ok14mgnfbp). After downloading and extracting both archives the folder structure should look like the following:

```
quaid/ 
|
 -- 
    Data/
    QC-reports/
    Coverage/
    Processed-reads/
    Read-mapping/
    Variant-calling-iVar/
    Variant-calling-LoFreq/
    Variant-calling-combined/
    MSA_precomputed/
```

Now, in order to run the data processing pipeline you will need to execute the following two lines in the terminal:

```
bwa index SARS-CoV-2-reference.fasta
./run_analyses.sh TST08082022
./run_analyses.sh TST08092022
```

This step takes about 17 minutes on an Intel MacBook.

Once the process completes, to analyze the resulting variant calls and assign VoC information you will need to run the following command:

```
mkdir output
python quaid.py MSA_precomputed/vdb_lineage_df_week.csv MSA_precomputed/metadata.tsv ./Variant-calling-combined ./Coverage --concat-output --occurence-cutoff 2 --inclusion-cutoff 0.5 --exclusion-cutoff 0.5 --time-window 4 --level 4 --verbose
```

This step takes about 4.5 minutes on an Intel MacBook.

Finally you can inspect the resulting output produced, and compare it (for example using `diff` command) with the pre-computed output file available [here](https://rice.box.com/s/qan1tcn2qfepip3z38p0qksx7l0eek04).

## Manuscript

You can find the manuscript describing QuaID and corresponding results at [medRxiv:10.1101/2021.09.08.21263279v3](https://www.medrxiv.org/content/10.1101/2021.09.08.21263279v3).
